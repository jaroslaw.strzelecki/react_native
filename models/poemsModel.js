class Poems {
    constructor(
        id,
        tagIds,
        title,
        author,
        authorImgUrl,
        content,

    ) {
        this.id=id;
        this.tagIds = tagIds;
        this.title=title;
        this.author=author;
        this.authorImgUrl=authorImgUrl;
        this.content=content;
    }

}

export default Poems;